document.addEventListener('DOMContentLoaded', () => {
const diceArray= [
    {
        name: 'one',
        img: 'img/dice1.png',
    },
    {
        name: 'two',
        img: 'img/dice2.png',
    },
    {
        name: 'three',
        img: 'img/dice3.png',
    },
    {
        name: 'four',
        img: 'img/dice4.png',
    },
    {
        name: 'five',
        img: 'img/dice5.png',
    },
    {
        name: 'six',
        img: 'img/dice6.png',
    },
]

//Declaración de Variables
const player1ChoiceDisplay = document.getElementById('player1')
const player2ChoiceDisplay = document.getElementById('player2')
const resultDisplay = document.getElementById("resultDisplay");
var player1Choice
var player2Choice

//Función para imprimir la eleccion de los jugadores
generateplayer1ChoiceDisplay()
generateplayer2ChoiceDisplay()
result()

//Función para eleccion de jugadores automatico
function generateplayer1ChoiceDisplay(){
    const player1ChoiceDisplay = document.getElementById('player1')
    const randomNumber = Math.floor (Math.random() * diceArray.length) + 1
    if (randomNumber === 1){
        player1ChoiceDisplay.setAttribute('src', 'img/dice1.png')
        player1Choice = 1;
    }
    if (randomNumber === 2){
        player1ChoiceDisplay.setAttribute('src', 'img/dice2.png')
        player1Choice = 2;
    }
    if (randomNumber === 3){
        player1ChoiceDisplay.setAttribute('src', 'img/dice3.png')
        player1Choice = 3;
    }
    if (randomNumber === 4){
        player1ChoiceDisplay.setAttribute('src', 'img/dice4.png')
        player1Choice = 4;
    }
    if (randomNumber === 5){
        player1ChoiceDisplay.setAttribute('src', 'img/dice5.png')
        player1Choice = 5;
    }
    if (randomNumber === 6){
        player1ChoiceDisplay.setAttribute('src', 'img/dice6.png')
        player1Choice = 6;
    }
}

function generateplayer2ChoiceDisplay() {
  const player2ChoiceDisplay = document.getElementById("player2");
  const randomNumber = Math.floor(Math.random() * diceArray.length) + 1;
  if (randomNumber === 1) {
    player2ChoiceDisplay.setAttribute("src", "img/dice1.png");
    player2Choice = 1;
  }
  if (randomNumber === 2) {
    player2ChoiceDisplay.setAttribute("src", "img/dice2.png");
    player2Choice = 2;
  }
  if (randomNumber === 3) {
    player2ChoiceDisplay.setAttribute("src", "img/dice3.png");
    player2Choice = 3;
  }
  if (randomNumber === 4) {
    player2ChoiceDisplay.setAttribute("src", "img/dice4.png");
    player2Choice = 4;
  }
  if (randomNumber === 5) {
    player2ChoiceDisplay.setAttribute("src", "img/dice5.png");
    player2Choice = 5;
  }
  if (randomNumber === 6) {
    player2ChoiceDisplay.setAttribute("src", "img/dice6.png");
    player2Choice = 6;
  }
}

function result(){
    if (player1Choice < player2Choice) {
        resultDisplay.innerHTML = 'PLAYER 2 WINS!'
    } else if (player1Choice == player2Choice) {
        resultDisplay.innerHTML = 'DRAW!'
    } else {
        resultDisplay.innerHTML = 'PLAYER 1 WINS!'
    } 
}






})

